Brief overview of directories 
=========================

 **api has 5 apis - these api's are deployed to AWS using cloudformation (devOps) 
   for convenience**
 
 1) create user - POST ( https://yyj0uj0y5l.execute-api.us-east-1.amazonaws.com/DEV/users )
 
 Sample request to create user
 `
 {
    "name": "Joe",
    "email": "joe@mail.com",
    "dob": "1992/11/13"
 }
 `

 2) delete user - (https://yyj0uj0y5l.execute-api.us-east-1.amazonaws.com/DEV/users/:id)
 3) list users - (https://yyj0uj0y5l.execute-api.us-east-1.amazonaws.com/DEV/users/list)
 4) get user - (https://yyj0uj0y5l.execute-api.us-east-1.amazonaws.com/DEV/users/3)
 
 5) db-initialize - However this service is not automated. As a improved we can add custom
 resource for initializing database. Currently, this is triggerred manually


 **devOps folder stores the cloudformation details - the stacks are in order**

 **Postman has json scripts for api calls **
 Please import this file to test functionality
 
 
