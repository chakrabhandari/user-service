'use strict';

let { User } = require('/opt/nodejs/models');
let utility = require('util');


exports.handler = async (event, context) => {

    try {
        let id = event.pathParameters.id;
        let user = await deleteUser(id);
        await readResponse( 200, utility.format("User with [id]:%s is deleted.", id));
    } catch (err) {
        console.log(utility.format('Error thrown: %s %s', err.statusCode, err));
        return `Internal Server ERROR occurred`;
    }

};

async function readResponse(code, body) {
    let res = {
        statusCode: code,
        body: JSON.stringify(body)
    }

    return res;
}

async function deleteUser(userId) {
    await User.destroy({where : { id : userId }})
        .catch((err) => {
            throw (
                {
                    code: err.statusCode || 501,
                    body: JSON.stringify(utility.format('DATABASE_ERROR: [%s] [%s], Error Thrown [%s]', 'delete', 'USER', err))
                }
            )
        });
}
