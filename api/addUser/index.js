'use strict';

let { User } = require('/opt/nodejs/models');
let { isValidBirthdate, emailvalidator } = require('/opt/nodejs/utils');
let utility = require('util');

let Ajv = require('ajv');
let ajv = Ajv({ allErrors: true })

let userSchema = require('./validation/user-schema');

exports.handler = async (event, context) => {

    let response = {
        statusCode: 200,
        body: JSON.stringify('User Created'),
    };

    try {
        let body = JSON.parse(event.body);

        let dob = body.dob;
        let email = body.email;
        let name = body.name;
        let schemaValid = ajv.validate(userSchema, body);

        if (!schemaValid) {
            response.statusCode = 400;
            response.body = JSON.stringify(ajv.errors);
            return response;
        }

        let split = dob.split('/');
        let date = new Date(split[2], split[1], split[0]); //Y M D

        if(!isValidBirthdate(date) ) {
            throw utility.format("Invalid Input dob %s ", dob);
        }

        if(!emailvalidator.validate(email)) {
            throw utility.format("Invalid Input email %s ",  email);
        }

        let user = {} ;
        user.name = name;
        user.email = email;
        user.dob = date;

        await createUser(user);
    } catch (err) {
        console.log(utility.format('Error thrown: %s %s', err.statusCode, err));
        return `Internal Server ERROR occurred`;
    }


    return response;
};


async function createUser(object) {
    let user = await User.create(object)
        .catch((err) => {
            throw (
                {
                    code: err.statusCode || 501,
                    body: JSON.stringify(utility.format('DATABASE ERROR %s %s %s', 'create', 'USER', err))
                }
            )
        });

    return user;
}
